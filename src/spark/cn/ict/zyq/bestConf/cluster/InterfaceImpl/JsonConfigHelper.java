package cn.ict.zyq.bestConf.cluster.InterfaceImpl;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mkhan on 2/27/2020.
 */
public class JsonConfigHelper {
    public static class Config {
        String name;
        boolean tunable;
        String suffix;
        String type;

        Config(String name, boolean tunable, String suffix, String type) {
            this.name = name;
            this.tunable = tunable;
            this.suffix = suffix;
            this.type = type;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("Name: ").append(name).append(" ");
            builder.append("Tunable: ").append(tunable).append(" ");
            builder.append("Suffix: ").append(suffix).append(" ");
            builder.append("Type: ").append(type).append(" ");
            return builder.toString();
        }
    }

    public static Map<String, Config> readJsonStream(InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
        try {
            return readConfigsArray(reader);
        } finally {
            reader.close();
        }
    }

    private static Map<String, Config> readConfigsArray(JsonReader reader) throws IOException {
        Map<String, Config> configs = new HashMap<>();

        reader.beginArray();
        while (reader.hasNext()) {
            Config conf = readConfig(reader);
            configs.put(conf.name, conf);
        }
        reader.endArray();
        return configs;
    }

    private static Config readConfig(JsonReader reader) throws IOException {
        String config = null;
        boolean tunable = false;
        String suffix = "";
        String type = null;

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("name")) {
                config = reader.nextString();
            } else if (name.equals("tunable")) {
                tunable = reader.nextBoolean();
            } else if (name.equals("suffix") && reader.peek() != JsonToken.NULL) {
                suffix = reader.nextString();
            } else if (name.equals("config_type")) {
                type = reader.nextString();
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return new Config(config, tunable, suffix, type);
    }
}
