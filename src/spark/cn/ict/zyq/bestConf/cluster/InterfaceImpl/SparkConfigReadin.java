/**
 * Copyright (c) 2017 Institute of Computing Technology, Chinese Academy of Sciences, 2017 
 * Institute of Computing Technology, Chinese Academy of Sciences contributors. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License. You
 * may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License. See accompanying
 * LICENSE file.
 */
package cn.ict.zyq.bestConf.cluster.InterfaceImpl;
import cn.ict.zyq.bestConf.cluster.Interface.ConfigReadin;
import cn.ict.zyq.bestConf.cluster.Utils.PropertiesUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

public class SparkConfigReadin implements ConfigReadin {


	private String filepath;
	@Override
	public void initial(String server, String username, String password, String localPath, String remotePath) {
		// TODO Auto-generated method stub
		this.filepath = remotePath;
	}
    
 
	@Override
	public void downLoadConfigFile(String fileName) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public HashMap modifyConfigFile(HashMap hm, String filepath) {
		HashMap ori = loadFileToHashMap(filepath);
		modify(ori, hm);
		return ori;
	}

	@Override
	public HashMap modifyConfigFile(HashMap hm) {
		HashMap ori = loadFileToHashMap(filepath);
		modify(ori, hm);
		return ori;
	}

	private HashMap loadFileToHashMap(String filePath) {
		HashMap hashmap = null;
		try {
			System.out.println("In Spark Config ReadIn, FilePath "+filePath);
			Properties pps = PropertiesUtil.GetAllProperties(filePath);
			hashmap = new HashMap((Map) pps);
			System.out.println("--------------------------");
                        //for(Object o : hashmap.entrySet()){
                        //Map.Entry e = (Map.Entry) o;
                        //Object key = e.getKey();
                        //Object value = e.getValue();
                        //System.out.println("to modify Key "+key+" value "+value);
                       //}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hashmap;
	}

	public void modify(HashMap toModify, HashMap updatedValues) {

	//	for(Object o : toModify.entrySet()){
	//		Map.Entry e = (Map.Entry) o;
	//		Object key = e.getKey();
	//		Object value = e.getValue();
	//	    System.out.println("to modify Key "+key+" value "+value);
	//	}
	//	System.out.println("--------------------------");
	//	for(Object o : updatedValues.entrySet()){
	//		Map.Entry e = (Map.Entry) o;
	//		Object key = e.getKey();
	//		Object value = e.getValue();
	//		System.out.println("updated Key "+key+" value "+value);
	//	}


		for (Object o : updatedValues.entrySet()) {
			Map.Entry entUpdated = (Map.Entry) o;
			Object tempObj = toModify.get(entUpdated.getKey().toString());
			if (tempObj != null) {
				if (!entUpdated.getValue().toString().equals("NaN")) {
					if (Double.parseDouble(entUpdated.getValue().toString()) < 1.0 && Double.parseDouble(entUpdated.getValue().toString()) != 0.0) {
						toModify.put(entUpdated.getKey().toString(), entUpdated.getValue().toString());
					} else if (Double.parseDouble(entUpdated.getValue().toString()) == 0.0) {
						toModify.put(entUpdated.getKey().toString(), Integer.parseInt("0"));
					} else {
						toModify.put(entUpdated.getKey().toString(), (int) Math.floor(Double.parseDouble(entUpdated.getValue().toString())));
					}
				}
			} else
				System.out.println(entUpdated.getKey().toString() + " doesn't exist in original list!");
		}
	}

	public static void main(String[] args){
		SparkConfigReadin sparkR = new SparkConfigReadin();
		SparkConfigWrite sparkW = new SparkConfigWrite();
		String filepath = "/home/mkhan/bestconf/scripts/conf/spark-conf.conf";
		String targetFilePath = "/home/mkhan/bestconf/scripts/conf/spark-conf-written.conf";
		HashMap hm = sparkR.loadFileToHashMap(filepath);
		Iterator it = hm.entrySet().iterator();
		sparkW.writetoConfigfile(hm, targetFilePath);
		while(it.hasNext()){
			Map.Entry entry_hm_ori = (Map.Entry)it.next();
			String key_hm_ori = entry_hm_ori.getKey().toString();
			System.out.println("key is : " + key_hm_ori);
			System.out.println("value is : " + entry_hm_ori.getValue());
		}
	}

}
