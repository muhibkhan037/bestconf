/**
 * Copyright (c) 2017 Institute of Computing Technology, Chinese Academy of Sciences, 2017 
 * Institute of Computing Technology, Chinese Academy of Sciences contributors. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License. You
 * may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License. See accompanying
 * LICENSE file.
 */
package cn.ict.zyq.bestConf.cluster.InterfaceImpl;
import cn.ict.zyq.bestConf.cluster.Interface.ConfigWrite;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SparkConfigWrite implements ConfigWrite {

	private HashMap<String, String[]> enums = new HashMap<>();
	private String[] spark_io_compression_codec = {"lz4", "lzf", "snappy", "zstd"};
	private String[] spark_serializer = {"org.apache.spark.serializer.JavaSerializer", "org.apache.spark.serializer.KryoSerializer"};
	private String filepath;
	@Override
	public void initial(String server, String username, String password, String localPath, String remotePath) {
		this.filepath = remotePath;
		enums.put("spark.io.compression.codec", spark_io_compression_codec);
		enums.put("spark.serializer", spark_serializer);
		System.out.println("FilePath of config in ConfigWrite "+filepath);
	}

	@Override
	public void uploadConfigFile() {

	}

	@Override
	public void writetoConfigfile(HashMap hm) {
		File file = new File(filepath);

		FileWriter fw = null;
		BufferedWriter bw = null;

		try {
			fw = new FileWriter(file.getAbsoluteFile());
			bw = new BufferedWriter(fw);
			InputStream input = new FileInputStream("/home/mkhan/sparktune/src/sampling/config/generated_default_configs_ns01.json");
			Map<String, JsonConfigHelper.Config> configMap = JsonConfigHelper.readJsonStream(input);

			for (Object o : hm.entrySet()) {
				Map.Entry entry = (Map.Entry) o;
				String key = entry.getKey().toString();
				String value = entry.getValue().toString();
				JsonConfigHelper.Config config = configMap.get(key);

				String CONFIG_TYPE_BOOLEAN = "BOOLEAN";
				String CONFIG_TYPE_CATEGORICAL = "CATEGORICAL";
				String CONFIG_TYPE_FLOAT = "FLOAT";

				if (config.type.equals(CONFIG_TYPE_BOOLEAN)) {
					double boolVal = Double.parseDouble(value);
					if (boolVal >= 0 && boolVal < 0.5) {
						bw.write(key + " " + "false\n");
					} else {
						bw.write(key + " " + "true\n");
					}
				} else if (config.type.equals(CONFIG_TYPE_CATEGORICAL)) {
					int index = Double.valueOf(value).intValue();
					bw.write(key + " " + enums.get(key)[index] + "\n");
				} else if (config.type.equals(CONFIG_TYPE_FLOAT)){
					double floatVal = Double.parseDouble(value);
					bw.write(key + " " + floatVal + "\n");
				} else {
					// config type int time and size
					int intValue = Double.valueOf(value).intValue();
					bw.write(key + " " + intValue + config.suffix +"\n");
				}


			}
			bw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void writetoConfigfile(HashMap hm, String filepath) {
		this.filepath = filepath;
		writetoConfigfile(hm);
	}


}
