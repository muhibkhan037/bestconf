#!/bin/bash
echo "Entern Number of tuning runs "
START=1
read END
cd ./scripts
pwd
./genData.sh
#$(./genData.sh)
echo input generated
cd ../
for (( c=$START; c<=$END; c++ ))
do
	echo "Tuning Run No $c"
	pwd
	ant run
	cd ./data
	res_dir="./res/${c}"
        mkdir -p $res_dir
	mv bestconf_iteration_result.yaml $res_dir
	mv iteration_confs $res_dir
	pwd
        ./clear.sh
	cd ../
done

