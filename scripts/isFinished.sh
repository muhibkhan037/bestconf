#!/usr/bin/env bash
input="./execution_status"
while read -r line
do
    if [[ "$line" == "Run done" ]]
    then
      echo "ok"
    else
      echo "not ok!"
    fi
done < "${input}"
