import os
import subprocess as sp
import sys
import fileinput
from pathlib import Path


SPARKBENCH_MAIN_HOME = os.environ.get('SPARKBENCH_HOME')
HIBENCH_HOME = os.environ.get('HIBENCH_HOME')
DATA_GEN_SCRIPT_NAME = 'gen_data.sh'
WORD_COUNT_DATAGEN_PATH = str(Path(f'{HIBENCH_HOME}/bin/workloads/micro/wordcount/prepare/prepare.sh'))
JOIN_DATAGEN_PATH = str(Path(f'{HIBENCH_HOME}/bin/workloads/sql/join/prepare/prepare.sh'))
SPARKBENCH_MAIN_WORKLOADS = ['Terasort', 'LogisticRegression', 'LinearRegression', 'SVM', 'MatrixFactorization',
                             'SVDPlusPlus', 'TriangleCount', 'DecisionTree', 'KMeans', 'LabelPropagation',
                             'PCA', 'ShortestPaths', 'SQL', 'StronglyConnectedComponent', 'WordCount', 'Join']

GRAPHX_APPS = ['PR', 'CC', 'PregelOp']


def modify_env_file_for_kmeans(workload, input_size: int):
    """
    Modify the env file for KMeans in code to change the dataset size generated for workload
    :param workload:
    :param input_size:
    """
    number_of_points = input_size * 1000000
    env_file_path = f'{SPARKBENCH_MAIN_HOME}/{workload}/conf/env.sh'
    with fileinput.FileInput(files=env_file_path, inplace=True) as f:
        for line in f:
            if 'NUM_OF_POINTS' in line:
                print(f'NUM_OF_POINTS={number_of_points}\n', end='')
            else:
                print(line, end='')


def modify_env_file_for_logistic_regression(workload, input_size: int):
    """
    Modify the env file for logistic regression in code to change the dataset size generated for workload
    :param workload:
    :param input_size:
    """
    number_of_points = input_size * 1000000
    print(f'Logistic Regression input {number_of_points}')
    env_file_path = f'{SPARKBENCH_MAIN_HOME}/{workload}/conf/env.sh'
    with fileinput.FileInput(files=env_file_path, inplace=True) as f:
        for line in f:
            if 'NUM_OF_EXAMPLES' in line:
                print(f'NUM_OF_EXAMPLES={number_of_points}\n', end='')
            else:
                print(line, end='')


def modify_env_file_for_svm(workload, input_size: int):
    """
    Modify the env file for SVM in code to change the dataset size generated for workload
    :param workload:
    :param input_size:
    """
    number_of_points = input_size * 1000000
    env_file_path = f'{SPARKBENCH_MAIN_HOME}/{workload}/conf/env.sh'
    with fileinput.FileInput(files=env_file_path, inplace=True) as f:
        for line in f:
            if 'NUM_OF_EXAMPLES' in line:
                print(f'NUM_OF_EXAMPLES={number_of_points}\n', end='')
            else:
                print(line, end='')


def modify_env_file_for_terasort(workload, input_size: int):
    """
    Modify the env file for Terasort in code to change the dataset size generated for workload
    :param workload:
    :param input_size:
    """
    number_of_records = input_size * 10000000
    env_file_path = f'{SPARKBENCH_MAIN_HOME}/{workload}/conf/env.sh'
    with fileinput.FileInput(files=env_file_path, inplace=True) as f:
        for line in f:
            if 'NUM_OF_RECORDS' in line:
                print(f'NUM_OF_RECORDS={number_of_records}\n', end='')
            else:
                print(line, end='')


def generate_wordcount_input(input_size: int):
    wordcount_conf_path = Path(HIBENCH_HOME) / 'conf/workloads/micro/wordcount.conf'
    input_size_in_bytes = int(input_size) * 1000000000
    with fileinput.FileInput(files=wordcount_conf_path, inplace=True) as f:
        for line in f:
            # we are changing the huge dataset size properties in the workload conf and keeping
            # the hibench conf set to use the huge dataset
            if 'hibench.wordcount.huge.datasize' in line:
                print(f'hibench.wordcount.huge.datasize                {input_size_in_bytes}\n', end='')
            else:
                print(line, end='')
    sp.run(WORD_COUNT_DATAGEN_PATH, shell=True, stderr=sp.STDOUT, universal_newlines=True)


def generate_join_input(input_size: int):
    conf_path = Path(HIBENCH_HOME) / '/conf/workloads/sql/join.conf'
    user_visit_count = int(input_size) * 1000000
    with fileinput.FileInput(files=conf_path, inplace=True) as f:
        for line in f:
            # we are changing the huge dataset size properties in the workload conf and keeping
            # the hibench conf set to use the huge dataset. However, as we are taking input this size
            # most probably will be a of a larger scale
            if 'hibench.join.huge.uservisits' in line:
                print(f'hibench.join.huge.uservisits                {user_visit_count}\n', end='')
            else:
                print(line, end='')
    sp.run(WORD_COUNT_DATAGEN_PATH, shell=True, stderr=sp.STDOUT, universal_newlines=True)


def generate_input(workload: str, input_size: int):
    print(f'Generate data for {workload}')
    if workload == 'Terasort':
        modify_env_file_for_terasort(workload, input_size)
    elif workload == 'KMeans':
        modify_env_file_for_kmeans(workload=workload, input_size=input_size)
    elif workload == 'LogisticRegression':
        modify_env_file_for_logistic_regression(workload=workload, input_size=input_size)
    elif workload == 'SVM':
        modify_env_file_for_svm(workload=workload, input_size=input_size)
    elif workload == 'WordCount':
        generate_wordcount_input(input_size)
        return
    elif workload == 'Join':
        generate_join_input(input_size)
        return
    else:
        raise Exception(f'Workload env file modification not implemented for {workload}!')

    gen_data_script_path = f'{SPARKBENCH_MAIN_HOME}/{workload}/bin/{DATA_GEN_SCRIPT_NAME}'
    print(gen_data_script_path)
    sp.run(gen_data_script_path, shell=True, stderr=sp.STDOUT, universal_newlines=True)


if __name__ == '__main__':
    workload_name = sys.argv[1]
    workload_input = sys.argv[2]
    print(workload_name, workload_input)
    if workload_name in SPARKBENCH_MAIN_WORKLOADS:
        workload_input = int(workload_input)
        generate_input(workload=workload_name, input_size=workload_input)
        print('ok')
    elif workload_name in GRAPHX_APPS:
        print('ok')
    else:
        print('not ok!')
