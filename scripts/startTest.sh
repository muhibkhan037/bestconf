#!/usr/bin/env bash
exec 6< workload_info
read workload_name <&6
read input_size <&6
exec 6<&-

conf_file_path="./conf/spark-conf.conf"
echo "Execute Application in background"
pyv="$(python -V 2>&1)"
echo "$pyv"
eval "$(conda shell.bash hook)"
conda activate bestconfig
condav="$(conda --version 2>&1)"
echo "$condav"
pyv="$(python -V 2>&1)"
echo "$pyv"
echo "workload $workload_name input $input_size"
#python startTestSource.py ${workload_name} ${input_size} ${conf_file_path}
nohup python startTestSource.py ${workload_name} ${input_size} ${conf_file_path} >/dev/null 2>&1 &
