import os
import sys
import time
import subprocess as sp
import setproctitle
from hdfs import InsecureClient
from pathlib import Path

USER_HOME = os.environ.get('HOME', '/home/mkhan')
USER_NAME = 'mkhan'
SPARK_HOME = os.environ.get('SPARK_HOME', '/home/mkhan/spark-custom/spark-2.4.1-fsu')
SPARK_SUBMIT_PATH = SPARK_HOME + '/bin/spark-submit'
SPARKBENCH_GRAPHX_APP_CLASS_NAME = '--class "GraphXWorkloadApp"'
SPARKBENCH_GRAPHX_APP_JAR_PATH = f'{USER_HOME}/Spark_Applications/' \
                             'GraphX-Workloads/target/scala-2.11/graphx-workloads_2.11-1.0.jar'
SPARKBENCH_GRAPHX_RDD_CACHING_LEVEL = 'mem_disk'
SPARKBENCH_GRAPHX_APP_ITERATION_COUNT_FOR_PR_AND_CC = 5
MASTER_NODE = os.environ.get('MASTER_NODE', 'localhost')
SPARK_MASTER = f'--master spark://{MASTER_NODE}:7077'
HDFS_MASTER = f'hdfs://{MASTER_NODE}:9000/user/{USER_NAME}'
GRAPHX_INPUT_PATH = f'{HDFS_MASTER}/GraphData'
SPARKBENCH_MAIN_HOME = os.environ.get('SPARKBENCH_HOME')
HIBENCH_HOME = os.environ.get('HIBENCH_HOME')

GRAPHX_APPS = ['PR', 'CC', 'PregelOp']
HIBENCH_APPS = ['WordCount', 'Join']
HDFS_CLIENT = None
TERASORT_HDFS_OUTPUT_SUCCESS_PATH = 'SparkBench/Terasort/Output/_SUCCESS'
TERASORT_HDFS_OUTPUT_DIR = 'SparkBench/Terasort/Output'

WORD_COUNT_EXECUTION_PATH = str(Path(f'{HIBENCH_HOME}/bin/workloads/micro/wordcount/spark/run.sh'))
JOIN_EXECUTION_PATH = str(Path(f'{HIBENCH_HOME}/bin/workloads/sql/join/spark/run.sh'))
HIBENCH_SPARK_CONF_FILE = str(Path(f'{HIBENCH_HOME}/conf/spark.conf'))
TRUNCATION_LINE = 'TRUNCATION_TOKEN'


def keep_content_up_to_line(original_file, condition_line):
    dummy_file = original_file + '.bak'
    # Open original file in read only mode and dummy file in write mode
    with open(original_file, 'r') as read_obj, open(dummy_file, 'w') as write_obj:
        # Line by line copy data from original file to dummy file
        for line in read_obj:
            # if current line matches the given condition write and break out
            if condition_line in line:
                write_obj.write(line + '\n')
                break
            else:
                write_obj.write(line)

    os.remove(original_file)
    os.rename(dummy_file, original_file)


def concat_files(target_file, source_file):
    with open(source_file) as source_text:
        to_concat = source_text.read()
    with open(target_file, 'a') as target_text:
        target_text.write(to_concat)


def remove_execution_files():
    if Path('execution_time').is_file():
        os.remove('execution_time')
    if Path('execution_status').is_file():
        os.remove('execution_status')


def get_hdfs_client():
    client = InsecureClient(f'http://{MASTER_NODE}:50070', user={USER_NAME})
    return client


def check_if_exists(client, path: str) -> bool:
    exists = client.status(hdfs_path=path, strict=False)
    return exists is not None


def delete_dir(client, path: str):
    return client.delete(hdfs_path=path, recursive=True)


def clear_terasort_output():
    global HDFS_CLIENT
    if HDFS_CLIENT is None:
        HDFS_CLIENT = get_hdfs_client()
    # print('Clean Terasort Output')
    output_dir_deleted = delete_dir(HDFS_CLIENT, TERASORT_HDFS_OUTPUT_DIR)
    if output_dir_deleted:
        print('Cleaned Terasort output')
    else:
        print('Terasort output directory does not exist!')


def check_terasort_success(hdfs_client):
    success = check_if_exists(hdfs_client, TERASORT_HDFS_OUTPUT_SUCCESS_PATH)
    return success


def get_main_benchmark_app_args(workload_name):
    app_input_path = f'{HDFS_MASTER}/SparkBench/{workload_name}/Input'
    app_output_path = f'{HDFS_MASTER}/SparkBench/{workload_name}/Output'
    if workload_name == 'Terasort':
        app_class_name = 'src.main.scala.terasortApp'
        terasort_jar_dir = Path(SPARKBENCH_MAIN_HOME) / f'{workload_name}/target'
        app_jar_path = f'--jars {terasort_jar_dir}/jars/guava-19.0-rc2.jar ' \
                       f'{terasort_jar_dir}/TerasortApp-1.0-jar-with-dependencies.jar'
        app_args = f'{app_input_path} {app_output_path}'
        return app_class_name, app_jar_path, app_args
    elif workload_name == 'KMeans':
        app_class_name = 'KmeansApp'
        app_jar_path = Path(SPARKBENCH_MAIN_HOME) / f'{workload_name}/target' / 'KMeansApp-1.0.jar'
        kmeans_cluster_count = 2
        kmeans_max_iterations = 10
        kmeans_num_runs = 4
        kmeans_rdd_storage_level = 'MEMORY_ONLY'
        app_args = f'{app_input_path} {app_output_path} {kmeans_cluster_count} ' \
                   f'{kmeans_max_iterations} {kmeans_num_runs} {kmeans_rdd_storage_level}'
        return app_class_name, app_jar_path, app_args
    elif workload_name == 'LogisticRegression':
        app_class_name = 'LogisticRegression.src.main.java.LogisticRegressionApp'
        app_jar_path = Path(SPARKBENCH_MAIN_HOME) / f'{workload_name}/target' / 'LogisticRegressionApp-1.0.jar'
        max_iterations = 10
        rdd_storage_level = 'MEMORY_AND_DISK'

        app_args = f'{app_input_path} {app_output_path}  ' \
                   f'{max_iterations} {rdd_storage_level}'

        return app_class_name, app_jar_path, app_args
    elif workload_name == 'SVM':
        app_class_name = 'SVM.src.main.java.SVMApp'
        app_jar_path = Path(SPARKBENCH_MAIN_HOME) / f'{workload_name}/target' / 'SVMApp-1.0.jar'
        max_iterations = 10
        rdd_storage_level = 'MEMORY_ONLY'

        app_args = f'{app_input_path} {app_output_path}  ' \
                   f'{max_iterations} {rdd_storage_level}'
        return app_class_name, app_jar_path, app_args
    else:
        raise Exception('Unknown workload!')


def get_submission_string(workload_name, input_size, conf_file_path):
    if workload_name in GRAPHX_APPS:
        input_path = f'{GRAPHX_INPUT_PATH}/{input_size}'
        if workload_name == 'PR':
            app_args = f'PageRank static {SPARKBENCH_GRAPHX_APP_ITERATION_COUNT_FOR_PR_AND_CC}'
        elif workload_name == 'CC':
            app_args = f'CC {SPARKBENCH_GRAPHX_APP_ITERATION_COUNT_FOR_PR_AND_CC}'
        elif workload_name == 'PregelOp':
            app_args = 'PregelOp'
        else:
            app_args = f'PageRank static {SPARKBENCH_GRAPHX_APP_ITERATION_COUNT_FOR_PR_AND_CC}'

        submission_string: str = f'{SPARK_SUBMIT_PATH} ' \
                                 f'{SPARKBENCH_GRAPHX_APP_CLASS_NAME} ' \
                                 f'{SPARK_MASTER} ' \
                                 f'--properties-file={conf_file_path} ' \
                                 f'{SPARKBENCH_GRAPHX_APP_JAR_PATH} ' \
                                 f'{input_path} {SPARKBENCH_GRAPHX_RDD_CACHING_LEVEL} {app_args}'
        return submission_string
    elif workload_name in HIBENCH_APPS:
        # truncate previous confs from spark conf file of hibench
        keep_content_up_to_line(HIBENCH_SPARK_CONF_FILE, TRUNCATION_LINE)
        # Add the current conf to the file
        concat_files(HIBENCH_SPARK_CONF_FILE, conf_file_path)
        if workload_name == 'WordCount':
            return WORD_COUNT_EXECUTION_PATH
        elif workload_name == 'Join':
            return JOIN_EXECUTION_PATH
    else:
        app_class_name, app_jar_path, app_args = get_main_benchmark_app_args(workload_name=workload_name)
        submission_string: str = f'{SPARK_SUBMIT_PATH} ' \
                                 f'--class {app_class_name} ' \
                                 f'{SPARK_MASTER} ' \
                                 f'--conf spark.app.input.size={input_size} ' \
                                 f'--properties-file={conf_file_path} ' \
                                 f'{app_jar_path} ' \
                                 f'{app_args}'
        return submission_string


def get_conf_dictionary_from_file(conf_file_path):
    conf_dict = {}
    with open(conf_file_path) as conf_file:
        lines = conf_file.readlines()
        lines = [line.strip() for line in lines]
    for line in lines:
        conf_name, conf_val = line.split(maxsplit=1)
        conf_dict[conf_name] = conf_val
    return conf_dict


if __name__ == '__main__':
    workload = sys.argv[1]
    workload_input = sys.argv[2]
    conf_file = sys.argv[3]

    setproctitle.setproctitle('spark-app-run')
    remove_execution_files()
    with open('execution_status', 'w') as file:
        file.write('Run start\n')
    submission = get_submission_string(workload_name=workload,
                                       input_size=workload_input,
                                       conf_file_path=conf_file)
    with open('execution_log', 'w') as log:
        # print(submission_string)
        if workload == 'Terasort':
            clear_terasort_output()
        conf_dict = get_conf_dictionary_from_file(conf_file_path=conf_file)
        with open(f'../data/iteration_confs', 'a+') as file:
            file.write(f'{conf_dict}\n')
        start = time.monotonic()
        sp.run(submission, shell=True, stdout=log, stderr=sp.STDOUT, universal_newlines=True)
        execution_time = str(time.monotonic() - start)
        with open('execution_time', 'w') as file:
            file.write(execution_time)
        if workload == 'Terasort':
            terasort_success = check_terasort_success(HDFS_CLIENT)
            if not terasort_success:
                with open('execution_time', 'w') as file:
                    file.write(str(-1))
        with open('execution_status', 'w') as file:
            file.write('Run done\n')
