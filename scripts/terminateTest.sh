#!/usr/bin/env bash

app_execution_pid="$(pgrep spark-app-run 2>&1)"
echo "${app_execution_pid}"

echo "Kill spark-app-run"
kill -9 $app_execution_pid && kill -9 $app_execution_pid && echo yes

spark_submit="$( jps | grep SparkSubmit | awk '{print $1}')"
echo "${spark_submit}"

kill -9 $spark_submit && kill -9 $spark_submit && echo yes
