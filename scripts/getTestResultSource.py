from pathlib import Path

if __name__ == '__main__':
    if Path('execution_time').is_file():
        with open('execution_time', 'r') as file:
            time = file.readline()
            print(time)
    else:
        print('Execution Time file does not exist!')
