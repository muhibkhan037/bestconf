#!/usr/bin/env bash
workloads=(PR CC KMeans LogisticRegression SVM Terasort)
echo "Specify workload from ${workloads[*]}"
read workload
echo "Enter Input"
read input_size
echo $workload > workload_info
echo $input_size >> workload_info
pyv="$(python -V 2>&1)"
eval "$(conda shell.bash hook)"
conda activate bestconfig
condav="$(conda --version 2>&1)"
echo "$condav"
pyv="$(python -V 2>&1)"
echo "$pyv"
python genDataSource.py ${workload} ${input_size}
